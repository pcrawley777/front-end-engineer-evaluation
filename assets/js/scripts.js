// Insert your token here for the api.
const appToken = "";

let table = $("#tableBody");
let bodyCounter = 0;
let bodyRows = 10;
let tableLength = 0;

// Create table on startup
$.ajax({
  url: "https://data.pa.gov/resource/gcnb-epac.json",
  type: "GET",
  data: {
    $limit: 5000,
    $$app_token: appToken,
  },
}).done(function (data) {
  createTable(data);
  tableLength = data.length;
});


// Get button ID's for events
const prev = $("#prev");
const next = $("#next");

prev.click(() => {
  $.ajax({
    url: "https://data.pa.gov/resource/gcnb-epac.json",
    type: "GET",
    data: {
      $limit: 5000,
      $$app_token: appToken,
    },
  }).done(function (data) {
    if (bodyCounter > 0) {
      $("#tableBody tr").remove();
      bodyCounter -= 10;
      bodyRows -= 10;
      createTable(data);
    }
  });
});

next.click(function () {
  $.ajax({
    url: "https://data.pa.gov/resource/gcnb-epac.json",
    type: "GET",
    data: {
      $limit: 5000,
      $$app_token: appToken,
    },
  }).done(function (data) {
    if (bodyCounter < tableLength) {
      $("#tableBody tr").remove();
      if (bodyCounter + 10 <= tableLength) {
        bodyCounter += 10;
        bodyRows += 10;
      }
      createTable(data);
    }
  });
});

// Loops 10 times to append data into the table body
function createTable(data) {
  for (let i = bodyCounter; i < data.length && i < bodyRows; i++) {
    table.append(
      "<tr><td class='county-name'>" +
        data[i].county +
        "</td><td>" +
        addCommas(data[i].county_population) +
        "</td><td>" +
        addCommas(data[i].fully_covered) +
        "</td><td>" +
        addCommas(data[i].rate_fully_covered) +
        "</td></tr>"
    );
  }
}

// Converting number pulled from api to string to add comma
function addCommas(x) {
    if (x == null) {
        return "NA";
    }
    
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function sortTable(n) {
  let table,
    rows,
    switching,
    i,
    x,
    y,
    shouldSwitch,
    dir,
    switchcount = 0;
  table = document.getElementById("tableBody");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
    no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
      first, which contains table headers): */
    for (i = 0; i < rows.length - 1; i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
        one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
        based on the direction, asc or desc: */

        // This is for the first col since it's always a string, no conversions needed.
      if (n === 0) {
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      } else {
        // converting the strings back to numbers to calculate order
        let tempX = parseFloat(x.innerHTML.replace(/,/g, ""));
        let tempY = parseFloat(y.innerHTML.replace(/,/g, ""));

        if (dir == "asc") {
          if (Number(tempX) > Number(tempY)) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        } else if (dir == "desc") {
          if (Number(tempX) < Number(tempY)) {
            // If so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /* If no switching has been done AND the direction is "asc",
        set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
